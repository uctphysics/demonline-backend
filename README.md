# DemOnline V2 (Backend) #

demonline2 is a MEAN stack rewrite of a collection of (hundreds of )static pages, acting as a database of all available lecture demonstration equipment in the UCT Physics department. V2 
makes use of a mongoDB database (populated by scrubbing static pages). The backend is written in node.js
### Technologies used ###

* node.js
* Express
* mongoDB (using Mongoose)